﻿using Newtonsoft.Json;

using System;
using System.Collections.Generic;
using System.Configuration;
using System.Diagnostics;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Security.Policy;
using System.ServiceProcess;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;

using static System.Net.WebRequestMethods;

namespace DynuUpdater
{
    public partial class DynuUpdater : ServiceBase
    {
        private bool dirty = true;
        private string prevIp = null;
        private Thread mainThread = null;
        private CancellationTokenSource cts = null;
        public DynuUpdater()
        {
            InitializeComponent();
        }
        protected override void OnStart(string[] args)
        {
            StartMainLoop();
        }
        protected override void OnStop()
        {
            StopMainLoop();
        }
        public void StopMainLoop()
        {
            if (cts != null && !cts.Token.IsCancellationRequested)
            {
                cts.Cancel();
            }
        }
        public void StartMainLoop()
        {
            cts = new CancellationTokenSource();
            mainThread = new Thread(() => { bool g = MainLoop(); });
            mainThread.Start();
            EventLog.Source = "DynuUpdater";
        }
        private bool MainLoop()
        {
            Exception ex = null;
            string domainToUpdate = ConfigurationManager.AppSettings["domain"];
            Log($"Keeping {domainToUpdate} updated with current internet IP address.");
            string _nic = ConfigurationManager.AppSettings["nic"];
            Log($"Using network interface {_nic}");
            this.nic = IPAddress.Parse(_nic);
            while (true)
            {
                Thread.Sleep(10000);
                if (cts.Token.IsCancellationRequested)
                {
                    return Abort($"operation cancelled");
                }
                if (!dirty)
                {
                    if (!getMyIp(out string myIp, out ex))
                    {
                        myIp = prevIp;
                        Log($"Failed to obtain current ip address.  Detail:{Environment.NewLine}{ex}");
                    }
                    if (prevIp != myIp)
                    {
                        dirty = true;
                        Log($"ip address changed from {prevIp} to {myIp}");
                    }
                }
                if (dirty)
                {
                    if (!GetDomainList(out DomainListResponse domainListResponse, out ex))
                    {
                        Log($"Failed to obtain domain list.  Detail:{Environment.NewLine}{ex}");
                    }
                    else
                    {
                        if (domainListResponse == null)
                        {
                            Log($"domain list response is null");
                        }
                        else
                        {
                            if (domainListResponse.statusCode != 200)
                            {
                                Log($"domain list response status code was {domainListResponse.statusCode}");
                            }
                            else
                            {
                                if (domainListResponse.domains == null)
                                {
                                    Log($"domain list is null");
                                }
                                else
                                {
                                    List<Domain> domains = domainListResponse.domains;
                                    Domain domain = domains.FirstOrDefault(k => k.name == domainToUpdate);
                                    if (domain == null)
                                    {
                                        return Abort($"did not find {domainToUpdate} in domain list");
                                    }
                                    if (!getMyIp(out string myIp, out ex))
                                    {
                                        myIp = prevIp;
                                        Log($"Failed to obtain current ip address.  Detail:{Environment.NewLine}{ex}");
                                    }
                                    else
                                    {
                                        if (prevIp != myIp)
                                        {
                                            Log($"current ip address is: {myIp}.  Domain ip address is set to {domain.ipv4Address}");
                                        }
                                        prevIp = myIp;
                                        if (domain.ipv4Address != myIp)
                                        {
                                            DomainUpdate update = new DomainUpdate()
                                            {
                                                ipv4Address = myIp,
                                                name = domainToUpdate,
                                            };
                                            if (!UpdateDomain(update, domain.id, out DomainUpdateResponse dur, out ex))
                                            {
                                                Log($"Failed to update domain.  Detail:{Environment.NewLine}{ex}");
                                            }
                                            else
                                            {
                                                if (dur == null)
                                                {
                                                    Log($"update domain response was null");
                                                }
                                                else
                                                {
                                                    if (dur.statusCode != 200)
                                                    {
                                                        Log($"update domain response status code was {dur.statusCode}");
                                                    }
                                                    else
                                                    {
                                                        string sDur = JsonConvert.SerializeObject(dur);
                                                        Log($"update response: {sDur}");
                                                        dirty = false;
                                                    }
                                                }
                                            }
                                        }
                                        else
                                        {
                                            Log($"domain ip address confirmed: {domain.ipv4Address}");
                                            dirty = false;
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }

        private bool Abort(string why)
        {
            cts.Cancel();
            Log($"aborted because: {why}");
            return false;
        }
        private bool GetDomainList(out DomainListResponse response, out Exception ex)
        {
            ex = null;
            response = null;
            try
            {
                var url = "https://api.dynu.com/v2/dns";
                string dlr = GetDynuClient(new Uri(url)).GetStringAsync(url).Result;
                response = JsonConvert.DeserializeObject<DomainListResponse>(dlr);
                return true;
            }
            catch (Exception e)
            {
                ex = e;
                return false;
            }

        }
        private bool UpdateDomain(DomainUpdate update, int id, out DomainUpdateResponse response, out Exception ex)
        {
            response = null;
            ex = null;
            try
            {
                string bod = JsonConvert.SerializeObject(update);
                Log($"sending update: {bod}");
                ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;
                StringContent body2 = new StringContent(bod, Encoding.UTF8, "application/json");
                string url = $"https://api.dynu.com/v2/dns/{id}";
                HttpResponseMessage res2 = GetDynuClient(new Uri(url)).PostAsync(url, body2, cts.Token).Result;
                string res3 = res2.Content.ReadAsStringAsync().Result;
                response = JsonConvert.DeserializeObject<DomainUpdateResponse>(res3);
                Log($"update sent");
                return true;
            }
            catch (Exception e)
            {
                ex = e;
                return false;
            }
        }
        private IPAddress nic { get; set; } = null;
        private IPEndPoint BindIPEndPoint(ServicePoint servicePoint, IPEndPoint remoteEndPoint, int retryCount)
        {
            return new IPEndPoint(nic, 0);
        }

        private HttpClient GetBasicClient(Uri address)
        {
            ServicePoint servicePoint = ServicePointManager.FindServicePoint(address);
            servicePoint.BindIPEndPointDelegate = BindIPEndPoint;

            //Will cause bind to be called periodically
            servicePoint.ConnectionLeaseTimeout = 0;

            HttpClient client = new HttpClient();
            //will cause bind to be called for each request (as long as the consumer of the request doesn't set it back to true!
            client.DefaultRequestHeaders.ConnectionClose = true;
            return client;

        }
        private HttpClient GetDynuClient(Uri address)
        {
            HttpClient client = GetBasicClient(address);
            client.DefaultRequestHeaders.Accept.Clear();
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            string key = ConfigurationManager.AppSettings["api_key"];
            client.DefaultRequestHeaders.Add("API-Key", key);
            return client;
        }
        private bool getMyIp(out string address, out Exception ex)
        {
            address = null;
            ex = null;
            try
            {
                string url = "http://checkip.dynu.com";
                string response = GetBasicClient(new Uri(url)).GetStringAsync(url).Result;
                Match match = Regex.Match(response, @"\b(?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\b");
                if (!match.Success)
                {
                    address = null;
                    return false;
                }
                address = match.Value;
                return true;
            }
            catch (Exception e)
            {
                ex = e;
                return false;
            }
        }
        private void Log(string what)
        {
            string _what = $"{DateTime.Now}: {what}";
            try
            {
                EventLog.WriteEntry(_what);
            }
            catch { }
            try
            {
                Console.WriteLine(_what);
            }
            catch { }
        }
    }
    public class DomainUpdate
    {
        public string name { get; set; }
        public string group { get; set; }
        public string ipv4Address { get; set; }
        public string ipv6Address { get; set; }
        public int ttl { get; set; }
        public bool ipv4 { get; set; }
        public bool ipv6 { get; set; }
        public bool ipv4WildcardAlias { get; set; }
        public bool ipv6WildcardAlias { get; set; }
        public bool allowZoneTransfer { get; set; }
        public bool dnssec { get; set; }
    }
    public class Domain
    {
        public int id { get; set; }
        public string name { get; set; }
        public string unicodeName { get; set; }
        public string token { get; set; }
        public string state { get; set; }
        public string group { get; set; }
        public string ipv4Address { get; set; }
        public object ipv6Address { get; set; }
        public int ttl { get; set; }
        public bool ipv4 { get; set; }
        public bool ipv6 { get; set; }
        public bool ipv4WildcardAlias { get; set; }
        public bool ipv6WildcardAlias { get; set; }
        public DateTime createdOn { get; set; }
        public DateTime updatedOn { get; set; }
    }
    public class DomainUpdateResponse
    {
        public int statusCode { get; set; }
        public string type { get; set; }
        public string message { get; set; }
    }
    public class DomainListResponse
    {
        public int statusCode { get; set; }
        public List<Domain> domains { get; set; }
    }
}
