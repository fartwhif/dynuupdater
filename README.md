# DynuUpdater

A windows service that Keeps a dynu.com domain updated with the machine's current internet accessible IP address.  It makes calls to the [address reflector](http://checkip.dynu.com) to get the WAN address, and to the [dynu.com API](https://www.dynu.com/Support/API) to confirm and update the domain settings.

## Installation

1. Copy the contents of the bin/debug folder to C:\Program Files\DynuUpdater
2. Open an elevated command prompt
3. issue command `cd "C:\Program Files\DynuUpdater"`
4. issue command `C:\Windows\Microsoft.NET\Framework64\v4.0.30319\installutil.exe DynuUpdater.exe`
5. open config file `DynuUpdater.exe.config` and modify apikey and domain, save the file
6. open services
7. find DynuUpdater in services list
8. set it to automatic (optional)
9. start the service
10. open the event viewer
11. search for recent application log messages for DynuUpdater, it should have said "started" and mentioned your domain.
